import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Profile from './pages/Profile';
import EditProfile from './pages/ProfileEdit';
import ProfileChangePass from './pages/ProfileChangePass';
import Profiles from './pages/Profiles';
import ProfileView from './pages/ProfileView';
import MyOrders from './pages/MyOrders';
import MyOrdersHistory from './pages/MyOrdersHistory';
import Address from './pages/Address';
import Footer from './components/Footer';
import './App.css';
import {UserProvider} from './UserContext'





function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear()
  }

  //-------------------- To check if 
  useEffect(() =>{
    console.log(user); //result: _id and isAdmin
    console.log(localStorage); //result: accessToken
  }, [user])
  

  useEffect(() =>{
    fetch('http://localhost:4000/users/getUserDetails',{
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
      })
     .then(res => res.json())
     .then(data =>{
      console.log(data)
        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
        })
      }  
    })
  }, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Container>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/products" element={<Products/>}/>
          <Route exact path="/productView/:productId" element={<ProductView/>}/>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="/logout" element={<Logout/>}/>
          <Route exact path="/profile" element={<Profile/>}/>
          <Route exact path="/edit-profile" element={<EditProfile/>}/>
          <Route exact path="/edit-passsword" element={<ProfileChangePass/>}/>
          <Route exact path="/profiles" element={<Profiles/>}/>
          <Route exact path="/profileView/:profileId" element={<ProfileView/>}/>
          <Route exact path="/myOrders" element={<MyOrders/>}/>
          <Route exact path="/myOrdersHistory" element={<MyOrdersHistory/>}/>
          <Route exact path="/address" element={<Address/>}/>
          <Route path="*" element={<Error />} />
        </Routes>
        </Container>
      </Router>
      <Footer/>
    </UserProvider>
  );
}



export default App;
