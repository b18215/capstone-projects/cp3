import { useState, useEffect, useContext } from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import {regions, provinces, cities, barangays} from "select-philippines-address";

export default function Register(){

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate();
	

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


	const [regionData, setRegion] = useState([]);
	const [provinceData, setProvince] = useState([]);
	const [cityData, setCity] = useState([]);
	const [barangayData, setBarangay] = useState([]);

	const [regionAddr, setRegionAddr] = useState("");
	const [provinceAddr, setProvinceAddr] = useState("");
	const [cityAddr, setCityAddr] = useState("");
	const [barangayAddr, setBarangayAddr] = useState("");
	const [houseNumberAndStreet, setHouseNumberAndStreet] = useState('');

	const region = () => {
	    regions().then(response => {
	        setRegion(response);
	    });
	}

	const province = (e) => {
	    setRegionAddr(e.target.selectedOptions[0].text);
	    provinces(e.target.value).then(response => {
	        setProvince(response);
	        setCity([]);
	        setBarangay([]);
	    });
	}

	const city = (e) => {
	    setProvinceAddr(e.target.selectedOptions[0].text);
	    cities(e.target.value).then(response => {
	        setCity(response);
	    });
	}

	const barangay = (e) => {
	    setCityAddr(e.target.selectedOptions[0].text);
	    barangays(e.target.value).then(response => {
	        setBarangay(response);
	    });
	}

	const brgy = (e) => {
	    setBarangayAddr(e.target.selectedOptions[0].text);
	}

	useEffect(() => {
	    region()
	}, [])

	//------------------------------Conditions for Button [START]-----------------------------------------

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password === confirmPassword)){

			setIsActive(true);
		
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNumber, email, password]);

//------------------------------Conditions for Button [END]-----------------------------------------

//------------------------------Register User [START]-----------------------------------------
	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			//------------------------Swal Portion [START]-----------------------------
			if(data){
			Swal.fire({
				title: 'Registration Failed',
				icon: 'info',
				text: `${email} is already registered`	
			})
			} else {

				fetch('http://localhost:4000/users',{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNumber: mobileNumber,
						password: password,
						houseNumberAndStreet: houseNumberAndStreet,
						barangayAddr: barangayAddr,
						cityAddr: cityAddr,
						provinceAddr: provinceAddr,
						regionAddr: regionAddr
					})
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)
					if(data.email){
							Swal.fire({
							title: 'Registration complete.',
							icon: 'success',
							text: `${email} registration successful!`	
							})

							history("/login")
					} else {
							Swal.fire({
								title: 'Registration failed.',
								icon: 'error',
								text: 'Something went wrong'	
						})
					}
			})
		}
	
			//------------------------Swal Portion [END]-----------------------------

		})

		setFirstName('');
		setLastName('');
		setMobileNumber('');
		setEmail('');
		setHouseNumberAndStreet('');
		setBarangayAddr('');
		setCityAddr('');
		setProvinceAddr('');
		setRegionAddr('');
		setPassword('');


	}

	
	return (
		(user.id !== null) ?
		<Navigate to= "/products"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form className = "mt-5"onSubmit={e => registerUser(e)}>
			<div>
				<h1 id="pageTitle">Register</h1>
			</div>
			<Row>
			<Col>
			<Form.Group controlId="firstName">
				<Form.Label id="formlabel1">First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="lastName">
				<Form.Label id="formlabel2">Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="mobileNo">
				<Form.Label id="formlabel3">Mobile No.:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your 11-digit mobile number here"
					required
					value={mobileNumber}
					onChange={e => setMobileNumber(e.target.value)}
				/>
			</Form.Group>
			</Col>
			</Row>
			<Row>
			<Col>
			<Form.Group controlId="userEmail">
				<Form.Label id="formlabel4">Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please input your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="password">
				<Form.Label id="formlabel5">Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please input your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="confirmPassword">
				<Form.Label id="formlabel5">Confirm password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm password"
					required
					value={confirmPassword}
					onChange={e => setConfirmPassword(e.target.value)}
				/>
			</Form.Group>
			</Col>
			</Row>
			
			<Form.Group className="mt-2" controlId="deliveryAddress">
				<Form.Label id="formlabel6">Delivery Address:</Form.Label>
							
			        <Row>
			        <Col>
			        <Form.Group>
			        <Form.Label className="mb-0 pe-2">Region:</Form.Label>                  
			        <select onChange={province} onSelect={region}>
			            <option disabled>Select Region</option>
			            {
			                regionData && regionData.length > 0 && regionData.map((item) => <option
			                    key={item.region_code} 
			                    onChange={e => setRegionAddr(e.target.value)}
			                    value={item.region_code}>{item.region_name}</option>)
			            }
			        </select><br/>
			        </Form.Group>
			        </Col>
			        <Col>
			        <Form.Group>
			        <Form.Label className="mb-0 pe-2">District/Province:</Form.Label>                
			        <select onChange={city}>
			            <option disabled>Select Province</option>
			            {
			                provinceData && provinceData.length > 0 && provinceData.map((item) => <option
			                    key={item.province_code} 
			                    onChange={e => setProvinceAddr(e.target.value)}
			                    value={item.province_code}>{item.province_name}</option>)
			            }
			        </select><br/>
			        </Form.Group>
			        </Col>
			        <Col>
			        <Form.Group>
			        <Form.Label className="mb-0 pe-2">City/Minicipality:</Form.Label>
			        <select onChange={barangay}>
			            <option disabled>Select City</option>
			            {
			                cityData && cityData.length > 0 && cityData.map((item) => <option
			                    key={item.city_code} 
			                    onChange={e => setCityAddr(e.target.value)}
			                    value={item.city_code}>{item.city_name}</option>)
			            }
			        </select><br/>
			        </Form.Group>
			        </Col>
			        <Col>
			        <Form.Group>
			        <Form.Label className="mb-0 pe-2">Barangay:</Form.Label>
			        <select onChange={brgy}>
			            <option disabled>Select Barangay</option>
			            {
			                barangayData && barangayData.length > 0 && barangayData.map((item) => <option
			                    key={item.brgy_code} 
			                    onChange={e => setBarangayAddr(e.target.value)}
			                    value={item.brgy_code}>{item.brgy_name} 
			                    </option>)
			            }
			        </select>
			        </Form.Group>
			        </Col>
			        </Row>
			        <Row className="mt-3">
			        <Col>
			        <Form.Group id="form1">
			            <Form.Label className="mb-0">House No. & Street Name: </Form.Label>
			            <Form.Control 
			                type="text"
			                placeholder="Enter house no. & street name"
			                required
			                value={houseNumberAndStreet}
			                onChange={e => setHouseNumberAndStreet(e.target.value)}
			            />
			        </Form.Group>
			        </Col>
			        <Col>
			        <Form.Group  className="mt-3">
			            <Form.Label className="mb-0 pe-2">Delivery Address: </Form.Label>
			            {houseNumberAndStreet}, {barangayAddr}, {cityAddr}, {provinceAddr}, {regionAddr}		   
			        </Form.Group>
			        </Col>
			        </Row>
			</Form.Group>

  		 {
          <Button
            type='submit'
            id='regSubmitBtn'
            className='mt-3 mb-3'
            disabled={isActive ? false : true}
          >
            Submit
          </Button>
        }
		</Form>
		</>

	)	
}
