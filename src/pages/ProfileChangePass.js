import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ProfileChangePass(profileId){
	
	const {user, setUser} = useContext(UserContext)
	const history = useNavigate();

	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


//---------------------Conditions set on Edit Button----------------------
	useEffect(() => {
		if(password === confirmPassword){

			setIsActive(true); 

		
		} else {
			setIsActive(false);
			;
		}
	}, [password, confirmPassword]); //the effect will be triggered for this portion

	

//------------------------------------Function to Update Profile--------------------------------------------

	function UpdateProfile(e) {

		e.preventDefault();

//---------------------Conditions set on Button----------------------



//-------------------Edit Profile Password------------------------

	fetch('http://localhost:4000/users/updateUserPassword',{
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			password: password
		})
	})
	.then(res => res.json())
	.then(data =>{
		console.log(data)


			if(data){
			Swal.fire({
				title: 'Password updated!',
				icon: 'success',
				})

				history("/profile")

			} else { //Note: to take effect, ensure userControllers (in backend) returns boolean (false) in LOG-IN Section.
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'						
				})
			}
	

			setPassword(data.password)

	})


	
	}

//-------------------------------------Function to update profile end---------------------------------------



//------------------------------------Output-----------------------------------------------

	return (
			<Container id="changePassContainer">
			<div><h1 id="pageTitle6" className="mt-5 pt-5">Change Password</h1></div>
			<Form id="formChangePass" onSubmit={e => UpdateProfile(e)}>
			<Form.Group controlId="password">
				<Form.Label >New password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please input your new password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="confirmPassword">
				<Form.Label>Confirm password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm your new password"
					required
					value={confirmPassword}
					onChange={e => setConfirmPassword(e.target.value)}
				/>
			</Form.Group>

  		 {
          <Button
            type='submit'
            id='buttonChangePass'
            className='mt-3 mb-3'
            disabled={isActive ? false : true}
          >
            Update Password
          </Button>
        }
		</Form>
		</Container>



	)
}