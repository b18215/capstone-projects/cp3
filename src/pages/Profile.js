import {useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Profile(){

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNumber, setMobileNumber] = useState("");

	const [regionAddr, setRegionAddr] = useState("");
	const [provinceAddr, setProvinceAddr] = useState("");
	const [cityAddr, setCityAddr] = useState("");
	const [barangayAddr, setBarangayAddr] = useState("");
	const [houseNumberAndStreet, setHouseNumberAndStreet] = useState('');

	//-------------------View Profile------------------------
	useEffect((token) =>{
		fetch('http://localhost:4000/users/getUserDetails',{
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNumber(data.mobileNumber)
			setHouseNumberAndStreet(data.houseNumberAndStreet)
			setBarangayAddr(data.barangayAddr)
			setCityAddr(data.cityAddr)
			setProvinceAddr(data.provinceAddr)
			setRegionAddr(data.regionAddr)
		})

	},[])
	
	return(
		<Container className="mt-5">
			<Row className="mt-5">
				<Col className="px-0" lg={{span: 6, offset: 3}}>
					<h1 id="pageTitle4"className="pb-1 mt-5">My Profile</h1>
					<Card id="profileRow">
						<Card.Body >
							<Button as={ Link } to="/edit-profile" id="button-myprofile" className="me-5" variant="primary">Edit Profile</Button>
							<Button as={ Link } to="/edit-passsword" id="button-changePassword" className="ms-5 me-0" variant="primary">Change Password</Button>

						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>	
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title1" className="pb-1">First Name:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{firstName}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>	
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title2" className="pb-1">Last Name:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{lastName}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>	
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title3" className="pb-1">Email Address:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{email}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>	
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title4" className="pb-1">Mobile Number:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{mobileNumber}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>	
				<Col className="px-0" lg={{span: 6, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title5" className="pb-1">Delivery Address:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title6" className="pb-1">House Number/Street Name:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{houseNumberAndStreet}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title7" className="pb-1">Barangay/Village:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{barangayAddr}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title8" className="pb-1">City/Municipality:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{cityAddr}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title9" className="pb-1">Province / District:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{provinceAddr}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Row>
				<Col className="px-0" lg={{span: 3, offset: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle id="Title10" className="pb-1">Region:</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
				<Col className="px-0 mx-0" lg={{span: 3}}>
					<Card id="profileRow">
						<Card.Body>
							<Card.Subtitle className="pb-1">{regionAddr}</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)
}
