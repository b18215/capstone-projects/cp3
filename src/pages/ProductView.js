import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import { QuantityPicker } from "react-qty-picker";
import Select from "react-dropdown-select";
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import UploadCard from '../components/UploadCard';

export default function ProductView(e){



	const {user} = useContext(UserContext);
	const history = useNavigate();

	//retrieves the productId passed in the URL
	const {productId} = useParams();

	const [productName, setProductName] = useState("");
	const [productCode, setProductCode] = useState("");
	const [price, setPrice] = useState("0");
	const [size, setSize] = useState("");
	const [quantity, setQuantity] = useState("1");
	const [total, setTotal] = useState(price * quantity);

	//Qty selector
	const value = 1
	const darta = [
	  {
	    max: 99

	  }
	];

	//Dropdown-------------------------------------
	const options = [{ label: "Black", value: "Black" }, { label: "Red", value: "Red" }, { label: "Blue", value: "Blue" }, { label: "None", value: "None" }];

	const stampCollections = [
	{ label: "Received-Stamp-001", value: "Received-Stamp-001" }, 
	{ label: "Received-Stamp-002", value: "Received-Stamp-002" }, 
	{ label: "Received-Stamp-003", value: "Received-Stamp-003" }, 
	{ label: "Superseded-Stamp-001", value: "Superseded-Stamp-001" },
	{ label: "Superseded-Stamp-002", value: "Superseded-Stamp-002" },
	{ label: "Copy-Stamp-001", value: "Copy-Stamp-001" },
	{ label: "Distribution-Stamp-001", value: "Distribution-Stamp-001" },
	{ label: "Cancelled-Stamp-001", value: "Cancelled-Stamp-001" },
	{ label: "None", value: "None" },
	];



	//---------------------------------------------


	function order(e) {

		
		

		fetch(`http://localhost:4000/users/createOrder/${productCode}`,{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				price: price,
				status: 'Added to Cart'
			})

		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data){
			Swal.fire({
				title: 'Added to cart',
				icon: 'success',
			
				})

				history ("/products"); //redirection to Products page after sucessfull enrollment. (another way to redirect to particular page)

			} else { //Note: to take effect, ensure userControllers (in backend) returns boolean (false) in LOG-IN Section.
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'						
				})
			}
		})
	}
	console.log(price)
	console.log(productCode)


	useEffect(() =>{
		console.log(productId)

		fetch(`http://localhost:4000/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setProductName(data.productName)
			setPrice(data.price)
			setProductCode(data.productCode)
			setSize(data.size)
			setQuantity(data.quantity)
			setTotal(data.total)
			console.log(data.price)
			console.log(data.quantity)
			console.log(quantity)
			console.log(setQuantity)
		})

	}, [productId])



	// //Syntax: const [getter, setter] = useState(initialValueofGetter)
/*	const [count, setCount] = useState(0)
	const [seats, setSeats] = useState(30)
	const [isOpen, setIsOpen] = useState(false)

	const counter = () =>{
		if (seats > 0) {
			setCount(count + 1);
			console.log('Enrollees: ' + count);

			setSeats(seats - 1);
			console.log('Seats: ' + seats);
		} else {
			alert("Last seat taken");
			setIsOpen(true);
		};
	}
	useEffect(() =>{
		if(seats === 0){
			alert("Last seat taken");
			setIsOpen(true)
		}
	}, [seats])*/

	return(


		<Container id="productViewContainer" className="mt-5 pt-5">
			<div>
				<h1 id="pageTitle">Product Details</h1>
			</div>

		<Row  className="mb-5 pb-5" id="productViewRow">
			<Col id="productViewCol">
				<Col id="productViewTitle" classproductName="mb-4" lg={{span: 3}}>
					<Card classproductName="productCardHighlight">
						<Card.Body >
							<Card.Subtitle id="productCardHeader">Name</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>

				<Col id="productViewRow" classproductName="mb-4" lg={{span: 3}}>
					<Card id="productRow2">
						<Card.Body>
							<Card.Subtitle>{productName}</Card.Subtitle>
						</Card.Body>
					</Card>	
				</Col>
			</Col>

			<Col id="productViewCol">			
					<Col id="productViewTitle" classproductName="mb-4" lg={{span: 2}}>
						<Card classproductName="productCardHighlight">
							<Card.Body>
								<Card.Subtitle id="productCardHeader1">Code/Color</Card.Subtitle>
							</Card.Body>
						</Card>
					</Col>
					<Col id="productViewRow" classproductName="mb-4" lg={{span: 2}}>
						<Card id="productRow2">
							<Card.Body>
								<Card.Text>{productCode}</Card.Text>
								<Select 
								placeholder="Pre-Inked Color"
								single options={options} required 
								/>
							</Card.Body>
						</Card>	
					</Col>
		
			</Col>

			<Col id="productViewCol">
				<Col id="productViewTitle" classproductName="mb-4" lg={{span: 1}}>
					<Card classproductName="productCardHighlight">
						<Card.Body>
							<Card.Subtitle id="productCardHeader">Size</Card.Subtitle>
						</Card.Body>
					</Card>	
				</Col>
				<Col id="productViewRow" classproductName="mb-4" lg={{span: 1}}>
					<Card id="productRow3">
						<Card.Body>
							<Card.Text>{size}</Card.Text>
						</Card.Body>
					</Card>		
				</Col>
			</Col>

			<Col id="productViewCol">
			<Col id="productViewTitle" classproductName="mb-4" lg={{span: 2}}>
				<Card classproductName="productCardHighlight">
					<Card.Body>
						<Card.Subtitle id="productCardHeader">Price</Card.Subtitle>
					</Card.Body>
				</Card>	
			</Col>
			<Col id="productViewRow" classproductName="mb-4" lg={{span: 2}}>
				<Card id="productRow2">
					<Card.Body>
						<Card.Text
						onChange={setPrice}>
						PHP {price}</Card.Text>
					</Card.Body>
				</Card>	
			</Col>
			</Col>

			<Col id="productViewCol">
			<Col id="productViewTitle" classproductName="mb-4" lg={{span: 2}}>
				<Card classproductName="productCardHighlight">
					<Card.Body>
						<Card.Subtitle 
						id="productCardHeader">Quantity
						</Card.Subtitle>
					</Card.Body>
				</Card>	
			</Col>
			<Col id="productViewRow" classproductName="mb-4" lg={{span: 2}}>
				<Card id="productRow2" >
					<Card.Body >
						<div >
						  {darta.map((data) => (
						    <div className="App">
						      <QuantityPicker 
						      	smooth min={data.max}
						      	required	
						      	value={quantity}						      	
						      	onChange={e => setQuantity(e.target.value)}
						      	width='8rem'					    
						      	 />						      	 
						    </div>
						  ))}
						</div>
					</Card.Body>
				</Card>	
			</Col>
			</Col>

			<Col id="productViewCol">
			<Col id="productViewTitle" classproductName="mb-4" lg={{span: 2}}>
				<Card classproductName="productCardHighlight">
					<Card.Body>
						<Card.Subtitle id="productCardHeader">Total</Card.Subtitle>
					</Card.Body>
				</Card>	
			</Col>
			<Col id="productViewRow" classproductName="mb-4" lg={{span: 2}}>
				<Card id="productRow2">
					<Card.Body>
						<Card.Text>PHP {total}</Card.Text>
					</Card.Body>
				</Card>	
			</Col>
			</Col>

			<Col id="productViewCol">
			<Col id="productViewTitle" classproductName="mb-4" lg={{span: 2}}>
				<Card classproductName="productCardHighlight">
					<Card.Body>
						<Card.Subtitle id="productCardHeader">Choose Stamp Design</Card.Subtitle>
					</Card.Body>
				</Card>	
			</Col>
			<Col id="productViewRow" classproductName="mb-4" lg={{span: 2}}>
				<Card id="productRow2">
					<Card.Body>
						<p id="option1">Option 1:</p>
						<Select 
						placeholder="Select design from our collections"
						single options={stampCollections} required 
						/>
						
						<p id="option2">Option 2:</p>
						<UploadCard/>

					</Card.Body>
				</Card>	
			</Col>
			</Col>

			<Col id="productViewCol">
			<Col id="productViewTitle" classproductName="mb-4" lg={{span: 2}}>
				<Card classproductName="productCardHighlight">
					<Card.Body className="mx-0">
						<Card.Subtitle id="productCardHeader">Purchase</Card.Subtitle>
					</Card.Body>
				</Card>
			</Col>
			<Col id="productViewRow" classproductName="mb-4" lg={{span: 2}}>
				<Card id="productRow2">
					<Card.Body>
					{ user.id !== null ?
						<Button type='submit' id='purchaseBtn' onClick={() => order(productId)}>Add to cart</Button>
						:
						<Link id='regSubmitBtn2' className="btn btn-danger" to="/login">Register/Log in</Link>

					}
					</Card.Body>
				</Card>	
			</Col>
			</Col>



		</Row>
		</Container>


	)
}