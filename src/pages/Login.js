import { useState, useEffect, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Login(props){
	console.log(props)

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

/*	console.log(email);
	console.log(password);*/

	useEffect(() => {
		if(email !== '' && password !== ''){

			setIsActive(true);

		
		} else {
			setIsActive(false);
			;
		}
	}, [email, password]); //the effect will be triggered for this portion

	function loginUser(e) {

		e.preventDefault();



		//-------------------------Connecting to Database-------------------------------

		fetch('http://localhost:4000/users/login', { 
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
			.then(res => res.json())
			.then(data =>{
				console.log(data)

				if(typeof data.accessToken !== "undefined"){
					localStorage.setItem('token', data.accessToken)
					retrieveUserDetails(data.accessToken)
					//------------------------Swal Portion [START]-----------------------------
					Swal.fire({
						title: 'Login Sucessful',
						icon: 'success',
						text: `Logging-in as: ${email}`
					}) 

					
					} else { //Note: to take effect, ensure userControllers (in backend) returns boolean (false) in LOG-IN Section.
						Swal.fire({
							title: 'Authentication Failed',
							icon: 'error',
							text: 'Check your credentials'						
						})
					}
					//------------------------Swal Portion [END]-----------------------------
			})

		//-------------------------Connecting to Database end-------------------------------




		// localStorage.setItem('email', email)

	/*	setUser({
			email: localStorage.getItem('email')
		});*/

/*		alert(`${email} has been verified! Welcome back!`)
		setEmail('');
		setPassword('');
		console.log(email)
		console.log(password)*/
	}

	
	//-------Retrieve User Details---------------------
	const retrieveUserDetails = (token) =>{
		fetch('http://localhost:4000/users/getUserDetails',{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data =>{
				console.log(data)

				setUser({
							id: data._id,
							isAdmin: data.isAdmin
						})
		})
	}
	//-------Retrieve User Details [END]---------------------

	return (
		(user.id !== null) ?
		<Navigate to= "/products"/>
		:
		<>
		<h1 className = "mt-5 pt-5" id="pageTitle3">Login</h1>
		<Form id="regForm" className = "mt-5" onSubmit={e => loginUser(e)}>
			<Form.Group controlId="loginuserEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please input your email here to login"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="loginpassword">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please input your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
  		 {
          <Button
            variant={isActive ? 'success' : 'danger'}
            type='submit'
            id='loginsubmitBtn'
            className='mt-3 mb-3'
            disabled={isActive ? false : true}
          >
            Login
          </Button>
        }

        <p className="mb-1">Not yet registered? <Link to="/register">Register here</Link></p>
        <p className="mt-1">Forgot password? <Link to="/contact-us">Contact Us</Link></p>
		</Form>
		</>



	)
}
