// import ordersData from '../data/ordersData';
import {useEffect, useState} from 'react';
import OrderCard from '../components/OrderCard';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function MyOrders(){


	const [orders, setorders] = useState([])
	

	useEffect(() =>{
		fetch('http://localhost:4000/users/getOrders',{
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
      })
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setorders(data.map(order =>{
				return (
					<OrderCard key={order._id} orderProp={order}/>)
			}))
		})
	}, [])


	return(
	<>	
		<Row id="orderContainer2" className="pt-5 mt-5">
		<Card  id="profileRow">
			<Card.Body >
				<h1 id="pageTitle2">My Orders: <Button as={ Link } to="/MyOrdersHistory" id="buttonOrderHistory" className="ms-5 me-0" variant="primary">Order History</Button></h1>
				

			</Card.Body>
		</Card>
		</Row>
		<Container className="mt-4" id="orderContainer">
		<Row>
		<Col id="orderCardHighlightTitle" classorderName="mb-4" lg={{span: 3}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body >
					<Card.Subtitle id="orderCardHeader">Code</Card.Subtitle>
				</Card.Body>
			</Card>
		</Col>
		<Col id="orderCardColDate" classorderName="mb-4" lg={{span: 2}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body>
					<Card.Subtitle id="orderCardHeader">Date Ordered</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="orderCardHighlight2" classorderName="mb-4" lg={{span: 1}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body>
					<Card.Subtitle id="orderCardHeader">Price</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="orderCardColQty" classorderName="mb-4" lg={{span: 1}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body>
					<Card.Subtitle id="orderCardHeader">Qty.</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="orderCardColAmount" classorderName="mb-4" lg={{span: 1}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body>
					<Card.Subtitle id="orderCardHeader">Amount</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="orderCardColStatus" classorderName="mb-4" lg={{span: 2}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body className="mx-0">
					<Card.Subtitle id="orderCardHeader">Status</Card.Subtitle>
				</Card.Body>
			</Card>
		</Col>
		<Col id="orderCardColAction" classorderName="mb-4" lg={{span: 2}}>
			<Card classorderName="orderCardHighlight">
				<Card.Body className="mx-0">
					<Card.Subtitle id="orderCardHeader">Action</Card.Subtitle>
				</Card.Body>
			</Card>
		</Col>
		</Row>
		</Container>

		{orders}
		<Row id="totalContainer" className="pt-2">
		<Col id="totalContainer2">
		<Card  id="totalContainer">
			<Card.Body >
				<h4 id="pageTitle2">Total:</h4>
			</Card.Body >
		</Card>
		</Col>
		<Col id="totalContainer">
		<Card  id="totalContainer">
			<Card.Body >
				<h4 id="totalAmount1">PHP /Total Amount Here/</h4>
			</Card.Body >
		</Card>
		</Col>
		<Col id="totalContainer3">
		<Card  >
			<Card.Body id="cardCheckOut">
				<Button as={ Link } to="/MyOrdersHistory" id="checkOutButton" className="ms-5 me-0" variant="primary">Check-Out</Button>
			</Card.Body >
		</Card>
		</Col>
		</Row>
	</>
	)
}
