import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ProfileView(){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	//retrieves the profileId passed in the URL
	const {profileId} = useParams();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [deliveryAddress, setDeliveryAddress] = useState("");
	const [mobileNumber, setMobileNumber] = useState("");


	//-------------------Edit Profile------------------------
	const enroll = (profileId) =>{
		fetch('http://localhost:4000/users/updateUserDetails',{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				profileId: profileId
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data){
			Swal.fire({
				title: 'Profile updated!',
				icon: 'success',
				})

				history ("/profiles"); //redirection to Profiles page after sucessfull enrollment. (another way to redirect to particular page)

			} else { //Note: to take effect, ensure userControllers (in backend) returns boolean (false) in LOG-IN Section.
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'						
				})
			}
		})
	}

	useEffect((token) =>{
		console.log(profileId)

		fetch(`http://localhost:4000/users/getUserDetails`,{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNumber(data.mobileNumber)
			setDeliveryAddress(data.deliveryAddress)
		})

	}, [profileId])

	return(
		<Container className="mt-5">
			<Row className="mt-4">
				<Col lg={{span: 6, offset: 3}}>
					<Card className="mt-5">
						<Card.Body>
							<Card.Title className="pb-2">My Profile</Card.Title>
							<Card.Title className="pb-2">{firstName} {lastName}</Card.Title>

							<Card.Subtitle>Email: {email} | Mobile No.: {mobileNumber}</Card.Subtitle>
							<Card.Text>Delivery Address: {deliveryAddress}</Card.Text>

							{ user.id !== null ?
								<Button variant="primary" onClick={() => enroll(profileId)}>Update Profile</Button>
								:
								<Link className="btn btn-danger" to="/login">Log in</Link>

							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)
}