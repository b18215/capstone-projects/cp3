// import productsData from '../data/productsData';
import {useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';
import {Container, Row, Col, Card} from 'react-bootstrap';


export default function Products(){
/*	console.log(productsData);
	console.log(productsData[0]);
	
	
	//mapping or iteration of products as per the productData in mock-database
	const products = productsData.map(product =>{
		return(
			<ProductCard key = {product.id} productProp = {product}/>
			)
	})*/

	const [products, setProducts] = useState([])

	useEffect(() =>{
		fetch('http://localhost:4000/products') 
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setProducts(data.map(product =>{
				return (
					<ProductCard key={product._id} productProp={product}/>)
			}))
		})
	}, [])


	return(
	<>	
		<h1 className="pt-5 mt-5" id="pageTitle2">Products Available:</h1>
		{/*<ProductCard productProp = {productsData[0]}/>*/}
		<Container className="mt-4" id="container">
		<Row>
		<Col id="productCardHighlightTitle" classproductName="mb-4" lg={{span: 3}}>
			<Card classproductName="productCardHighlight">
				<Card.Body >
					<Card.Subtitle id="productCardHeader">Name</Card.Subtitle>
				</Card.Body>
			</Card>
		</Col>
		<Col id="productCardHighlight2" classproductName="mb-4" lg={{span: 2}}>
			<Card classproductName="productCardHighlight">
				<Card.Body>
					<Card.Subtitle id="productCardHeader">Code</Card.Subtitle>
				</Card.Body>
			</Card>
		</Col>
		<Col id="productCardHighlight2" classproductName="mb-4" lg={{span: 1}}>
			<Card classproductName="productCardHighlight">
				<Card.Body>
					<Card.Subtitle id="productCardHeader">Size</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight2" classproductName="mb-4" lg={{span: 2}}>
			<Card classproductName="productCardHighlight">
				<Card.Body>
					<Card.Subtitle id="productCardHeader">Price</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight2" classproductName="mb-4" lg={{span: 2}}>
			<Card classproductName="productCardHighlight">
				<Card.Body className="mx-0">
					<Card.Subtitle id="productCardHeader">Details</Card.Subtitle>
				</Card.Body>
			</Card>
		</Col>
		</Row>
		</Container>
		{products}
	</>
	)
}
