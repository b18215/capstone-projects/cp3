import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Error(){

	

	return(
		<Row id="pageNotFound" className = "mt-5">
			<Col className = "p-5">
				<h1>Page Not Found</h1>
				<Button id="error-button" className = "mt-2" as={Link} to="/" variant="primary">Return to homepage</Button>
			</Col>
		</Row>


	)
}