import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function EditProfile(profileId){
	
	const {user, setUser} = useContext(UserContext)
	const history = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNumber, setMobileNumber] = useState("");
	const [isActive, setIsActive] = useState(false);

	const [regionAddr, setRegionAddr] = useState("");
	const [provinceAddr, setProvinceAddr] = useState("");
	const [cityAddr, setCityAddr] = useState("");
	const [barangayAddr, setBarangayAddr] = useState("");
	const [houseNumberAndStreet, setHouseNumberAndStreet] = useState('');

//---------------------Conditions set on Edit Button----------------------
	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNumber !== ''){

			setIsActive(true); 

		
		} else {
			setIsActive(false);
			;
		}
	}, [firstName, lastName, mobileNumber]); //the effect will be triggered for this portion

	

//------------------------------------Function to Update Profile--------------------------------------------

	function UpdateProfile(e) {

		e.preventDefault();

//---------------------Conditions set on Button----------------------



//-------------------Edit Profile------------------------

	fetch('http://localhost:4000/users/updateUserDetails',{
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			mobileNumber: mobileNumber,
			houseNumberAndStreet: houseNumberAndStreet,
			barangayAddr: barangayAddr,
			cityAddr: cityAddr,
			provinceAddr: provinceAddr,
			regionAddr: regionAddr
		})
	})
	.then(res => res.json())
	.then(data =>{
		console.log(data)


			if(data){
			Swal.fire({
				title: 'Profile updated!',
				icon: 'success',
				})

				history("/profile")

			} else { //Note: to take effect, ensure userControllers (in backend) returns boolean (false) in LOG-IN Section.
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'						
				})
			}
	

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setMobileNumber(data.mobileNumber)
			setHouseNumberAndStreet(data.houseNumberAndStreet)
			setBarangayAddr(data.barangayAddr)
			setCityAddr(data.cityAddr)
			setProvinceAddr(data.provinceAddr)
			setRegionAddr(data.regionAddr)


	})


	
	}

//-------------------------------------Function to update profile end---------------------------------------

//----------------------------- Get User----------------------

useEffect((token) =>{
	fetch('http://localhost:4000/users/getUserDetails',{
		method: 'GET',
		headers: {
			'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(res => res.json())
	.then(data =>{
		console.log(data)

		setFirstName(data.firstName)
		setLastName(data.lastName)
		setMobileNumber(data.mobileNumber)
		setHouseNumberAndStreet(data.houseNumberAndStreet)
		setBarangayAddr(data.barangayAddr)
		setCityAddr(data.cityAddr)
		setProvinceAddr(data.provinceAddr)
		setRegionAddr(data.regionAddr)
	})

},[profileId])
//----------------------------- Get User [END]----------------------

//------------------------------------Output-----------------------------------------------

	return (
			<>
			<div><h1 id="pageTitle5" className="mt-5 pt-5">Edit Profile</h1></div>
			<Form id="formEditProfile" onSubmit={e => UpdateProfile(e)}>
			<Row>
			<Col>
			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			</Col>
			</Row>
			<Col>
			<Form.Group controlId="mobileNumber">
				<Form.Label>Mobile No.:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your 11-digit mobile number here"
					required
					value={mobileNumber}
					onChange={e => setMobileNumber(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<h5 className="mt-4">Delivery Address:</h5>
			<Row>
			<Col>
			<Form.Group controlId="houseNumberAndStreet">
				<Form.Label>House No./Street Name:</Form.Label>
				<Form.Control
					type="text"
					required
					value={houseNumberAndStreet}
					onChange={e => setHouseNumberAndStreet(e.target.value)}
				/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="barangayAddr">
				<Form.Label>Barangay:</Form.Label>
				<Form.Control
					type="text"
					required
					value={barangayAddr}
					onChange={e => setBarangayAddr(e.target.value)}
				/>
			</Form.Group>
			</Col>
			</Row>
			<Form.Group controlId="cityAddr">
				<Form.Label>City/Municipality:</Form.Label>
				<Form.Control
					type="text"
					required
					value={cityAddr}
					onChange={e => setCityAddr(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="provinceAddr">
				<Form.Label>Province/District:</Form.Label>
				<Form.Control
					type="text"
					required
					value={provinceAddr}
					onChange={e => setProvinceAddr(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="regionAddr">
				<Form.Label>Region:</Form.Label>
				<Form.Control
					type="text"
					required
					value={regionAddr}
					onChange={e => setRegionAddr(e.target.value)}
				/>
			</Form.Group>


  		 {
          <Button
            type='submit'
            id='button-editProfile'
            className='mt-3 mb-3'
            disabled={isActive ? false : true}
          >
            Update Profile
          </Button>
        }
		</Form>
		</>



	)
}
