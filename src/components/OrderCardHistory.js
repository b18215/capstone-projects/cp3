import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function OrderCardHistory({orderProp}){
	// console.log(productProp);
	// console.log(typeof productProp);


	// // Object destructuring
	const {productCode, purchaseDate, price, quantity, total, status/*, _id*/} = orderProp

	// // console.log(productName);
	


	return(
		<Container id="orderContainer">
		<Row>
		<Col id="productCardHighlightTitle" classproductName="mb-4" lg={{span: 3}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Subtitle>{productCode}</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Text>{purchaseDate}</Card.Text>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 1}}>
			<Card id="productRow3">
				<Card.Body>
					<Card.Text>PHP {price}</Card.Text>
				</Card.Body>
			</Card>		
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 1}}>
			<Card id="productRow3">
				<Card.Body>
					<Card.Text>{quantity}</Card.Text>
				</Card.Body>
			</Card>		
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 1}}>
			<Card id="productRow3">
				<Card.Body>
					<Card.Text>PHP {total}</Card.Text>
				</Card.Body>
			</Card>		
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Text>{status}</Card.Text>
				</Card.Body>
			</Card>	
		</Col>
		</Row>
		</Container>
	)
}