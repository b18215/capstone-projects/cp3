import {Container, Row, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function ProfileCard({profileProp}){

	// // Object destructuring
	const {_id, firstName, lastName, email, deliveryAddress} = profileProp

	

	return(
		<Container>
		<Row className="mt-5">
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>{firstName} {lastName}</Card.Title>
					<Card.Subtitle>ID Number: {_id} | Email Address: {email}</Card.Subtitle>
					<Card.Text>Customer delivery address: {deliveryAddress}</Card.Text>
					<Link className="btn btn-primary" to={`/profileView/${_id}`}>Edit Profile</Link>
				</Card.Body>
			</Card>	
		</Row>
		</Container>
	)
}