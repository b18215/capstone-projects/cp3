import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){

	

	return(
	
		<Row className = "p-5 stamp-bg">
			<Col className = "p-5">
				<h1 className ="banner1">Design Your Custom Rubber Stamps!</h1>
				<p className ="banner2">Browse our collections for designs or upload your preferred design to customize your rubber stamp. </p>
				<Button id="button1" as={Link} to="/collections" variant="primary" className="me-5">Stamps design collection</Button>
				<Button id="button2" as={Link} to="/login" variant="primary">Upload your design & order!</Button>
			</Col>
		</Row>



	)
}