import {Container, Row, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function ProfileCard({profileProp}){

	// // Object destructuring
	const {_id, firstName, lastName, email, deliveryAddress} = profileProp

	

	return(
		<Container>
		<Row classprofiName="mt-5">
			<Card classprofileName="cardHighlight p-3">
				<Card.Body>
					<Card.Title>{firstName} {lastName}</Card.Title>
					<Card.Subtitle>ID Number: {_id} | Email Address: {email}</Card.Subtitle>
					<Card.Text>Customer delivery address: {deliveryAddress}</Card.Text>
					<Link classprofileName="btn btn-primary" to={`/profileView/${_id}`}>Edit Profile</Link>
				</Card.Body>
			</Card>	
		</Row>
		</Container>
	)
}