import {Container, Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function ProductCard({productProp}){
	// console.log(productProp);
	// console.log(typeof productProp);


	// // Object destructuring
	const {productName, productCode, preInkedColor, size, price, isActive, createdOn, _id} = productProp
	// // console.log(productName);
	


	return(
		<Container id="container">
		<Row>
		<Col id="productCardHighlightTitle" classproductName="mb-4" lg={{span: 3}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Subtitle>{productName}</Card.Subtitle>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Text>{productCode}</Card.Text>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 1}}>
			<Card id="productRow3">
				<Card.Body>
					<Card.Text>{size}</Card.Text>
				</Card.Body>
			</Card>		
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Text>PHP {price}</Card.Text>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Link classproductName="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
				</Card.Body>
			</Card>	
		</Col>
		</Row>
		</Container>
	)
}