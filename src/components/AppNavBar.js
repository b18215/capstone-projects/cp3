// https://react-bootstrap.netlify.app/components/navbar/#navbars
import {useContext, useEffect, useState} from 'react'
import {Navbar, Container, Nav, Badge} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext'
import companyLogo from './Att/logo.png';

export default function AppNavBar(){

	// const [user, setUser] = useState(localStorage.getItem("email"))
	// console.log(user)

	const {user} = useContext(UserContext);
	console.log(user) // to check user currently logged-in

	const [firstName, setFirstName] = useState("");
	const [purchase, setPurchase] = useState("");
/*	const [addedToCart, setAddedToCart] = useState("");
	const [addedToCart1, setAddedToCart1] = useState("");*/


	const cartIcon = <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
  <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
</svg>

	
	useEffect((token) =>{

		
		fetch('http://localhost:4000/users/getUserDetails',{
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setFirstName(data.firstName)
			setPurchase(data.purchase)

/*			const addedToCart = purchase.filter((obj) => obj.status === "Added to Cart").length;
			const addedToCart1 = purchase.filter((obj) => obj.status === "Added to cart").length; 
			console.log(purchase);
			console.log(addedToCart + addedToCart1);*/

		})
		
	},[])
	

	return (
	<Navbar className= "bg-teal fixed-top" expand="lg">
	  <Container>
	    <div><img id="logo" src={companyLogo} as={Link} to="/register" width="40%" alt="LSA Stamps Logo"/></div>
	    <Navbar.Toggle className="bg-light" aria-controls="basic-navbar-nav" />
	    <Navbar.Collapse id="basic-navbar-nav">
	      <Nav className="me-auto">
	        <Nav.Link as={Link} to="/" className="text-white">Home</Nav.Link>
	        <Nav.Link as={Link} to="/products" className="text-white">Products</Nav.Link>

	        {
	        (user.id !== null) ?
	        <>
	        <Nav.Link  as={Link} to="/myOrders" className="text-white">Orders <Badge id="tag-count"> {cartIcon} {purchase.filter((obj) => obj.status === "Added to Cart").length}</Badge></Nav.Link>
	        <Nav.Link as={Link} to="/profile" className="text-white">Profile ({firstName})</Nav.Link>
	        <Nav.Link as={Link} to="/logout" className="text-white">Logout</Nav.Link>
	        </>
	        :
	        <>
	        <Nav.Link as={Link} to="/login" className="text-white">Login</Nav.Link>
	        <Nav.Link as={Link} to="/register" className="text-white">Register</Nav.Link>
	        </>
	    	}
	      </Nav>
	    </Navbar.Collapse>
	  </Container>
	</Navbar>


	)
}
