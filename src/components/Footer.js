
import React from 'react';



function Footer(){


return(

	<div id="footer" className="main-footer mt-5 pt-5">
		<div className="footer-middle">
			<div className="container">
				<div className="row">
					<div className="col-md-3 col-sm-6">
						<h4>Lorem ipsum</h4>
						<ul className="list-unstyled">
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>			
						</ul>
					  
					</div>
					<div className="col-md-3 col-sm-6">
						<h4>Lorem ipsum</h4>
						<ul className="list-unstyled">
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>			
						</ul>
					  
					</div>
					<div className="col-md-3 col-sm-6">
						<h4>Lorem ipsum</h4>
						<ul className="list-unstyled">
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>			
						</ul>
					  
					</div>
					<div className="col-md-3 col-sm-6">
						<h4>Lorem ipsum</h4>
						<ul className="list-unstyled">
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>
							<li>Lorem ipsum</li>			
						</ul>
					  
					</div>
				</div>
				{/*Footer Bottom*/}
				<div id="footerbanner" className="footer-bottom">
					<p className="text-xs-center"> 
					&copy;{new Date().getFullYear()} LSA Stamps® - All Rights Reserved
					</p>
				</div>
			</div>
		</div>
	</div>


  )
  }

  export default Footer;

