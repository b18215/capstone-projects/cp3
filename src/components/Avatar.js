import Avatar, { genConfig } from 'react-nice-avatar'

const config = genConfig(AvatarConfig?)

<Avatar style={{ width: '8rem', height: '8rem' }} {...config} />