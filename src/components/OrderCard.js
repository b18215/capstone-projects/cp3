import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { QuantityPicker } from "react-qty-picker";
import Select from "react-dropdown-select";

export default function OrderCard({orderProp}){
	// console.log(productProp);
	// console.log(typeof productProp);


	// // Object destructuring
	const {productCode, purchaseDate, price, quantity, total, status/*, _id*/} = orderProp

	
	//Qty selector
	var value = 1
	const darta = [
	  {
	    max: 1

	  }
	];

	//Dropdown-------------------------------------
	const options = [{ label: "Black", value: "Black" }, { label: "Red", value: "Red" }, { label: "Blue", value: "Blue" }, { label: "None", value: "None" }];


	return(
		<Container id="orderContainer">
		<Row>
		<Col id="productCardHighlightTitle" classproductName="mb-4" lg={{span: 3}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Subtitle>{productCode}</Card.Subtitle>
					<Select 
					id="dropdownlist1"
					placeholder="Stamp Color"
					single options={options} required 
					/>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="orderCardColDate" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Text>{purchaseDate}</Card.Text>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="productCardHighlight3" classproductName="mb-4" lg={{span: 1}}>
			<Card id="productRow3">
				<Card.Body>
					<Card.Text>PHP {price}</Card.Text>
				</Card.Body>
			</Card>		
		</Col>
		<Col id="orderCardColQty" classproductName="mb-4" lg={{span: 1}}>
		<Card id="productRow2">
			<Card.Body >
				<div >
				  {darta.map((data) => (
				    <div className="App">
				      <QuantityPicker id="qtyPicker" 
				      	smooth min={data.max}
				      	required	
				      	value = { quantity }
				      	width='8rem' 

				      	/>
				    </div>
				  ))}
				</div>
			</Card.Body>
		</Card>		
		</Col>
		<Col id="orderCardColAmount" classproductName="mb-4" lg={{span: 1}}>
			<Card id="productRow3">
				<Card.Body>
					<Card.Text>PHP {total}</Card.Text>
				</Card.Body>
			</Card>		
		</Col>
		<Col id="orderCardColStatus" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body>
					<Card.Text>{status}</Card.Text>
				</Card.Body>
			</Card>	
		</Col>
		<Col id="orderCardColAction" classproductName="mb-4" lg={{span: 2}}>
			<Card id="productRow2">
				<Card.Body >			
					{
					(status === 'Added to Cart') ?
					<>
					<Button as={ Link } to="/edit-profile" id="buttonEditOrder" variant="primary">Edit</Button>
					<Button as={ Link } to="/edit-passsword" id="buttonRemoveOrder" variant="primary">Remove</Button>
					</>
					:
					<>
					
					</>
					}		
				</Card.Body>
			</Card>	
		</Col>
		</Row>
		</Container>
	)
}